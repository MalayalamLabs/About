# About
The main aim of Malayalam Labs is to provide the necessary resources for translators of various projects. We hope this will be helpful to you.

# Helpful Links

- [SMC(Swanthanthra Malayalam Computing)](https://smc.org.in)
- [Olam.in (Dictionary)](https://olam.in/)
- [SMC (gitlab)](https://gitlab.com/smc)
- [Localization Lab](https://www.localizationlab.org/)
- [Localization Lab (Wiki-Malayalam)](https://wiki.localizationlab.org/index.php/Malayalam)
- [Malayalam WordNet](http://malayalamwordnet.cusat.ac.in/)
- [Mozilla Machinery](https://pontoon.mozilla.org/machinery/)
- [FOSS Community India (FCI) (Malayalam)](https://fci.fandom.com/wiki/%E0%B4%AE%E0%B4%B2%E0%B4%AF%E0%B4%BE%E0%B4%B3%E0%B4%82)
- [FCI (Malayalam Glossary)](https://fci.fandom.com/wiki/%E0%B4%AE%E0%B4%B2%E0%B4%AF%E0%B4%BE%E0%B4%B3%E0%B4%82/%E0%B4%97%E0%B5%8D%E0%B4%B2%E0%B5%8B%E0%B4%B8%E0%B5%8D%E0%B4%B8%E0%B4%B1%E0%B4%BF)
[malayalam Wiktionary](https://ml.wiktionary.org/wiki/%E0%B4%AA%E0%B5%8D%E0%B4%B0%E0%B4%A7%E0%B4%BE%E0%B4%A8_%E0%B4%A4%E0%B4%BE%E0%B5%BE)
